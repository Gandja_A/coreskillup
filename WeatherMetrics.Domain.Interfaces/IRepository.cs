﻿using System;
using System.Linq;

namespace WeatherMetrics.Domain.Interfaces
{
    public interface IRepository<T> : IDisposable
    {
        IQueryable<T> GetAll();
        void Create(T item);
        void Update(T item);
        void Delete(T item);
        void Save();
    }
}
