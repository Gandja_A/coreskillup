﻿using System.Collections.Generic;
using WeatherMetrics.Domain.Core.Models;

namespace WeatherMetrics.Domain.Interfaces
{
    public interface IUserQueryStatement
    {
        List<City> GetAllUserCities(string userName);
    }
}