﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;


namespace WeatherMetrics.Domain.Core
{
    public class ApplicationDbContextFactory : IDbContextFactory<ApplicationContext>
    {
        public ApplicationContext Create(DbContextFactoryOptions options)
        {
            var builder = new DbContextOptionsBuilder<ApplicationContext>();
             builder.UseSqlServer(@"Data Source = GANDJA\SQLEXPRESS; Initial Catalog = WeatherMetrics; Integrated Security = True; MultipleActiveResultSets=true");
             //builder.UseMySql("server=localhost; userid=root;password='admin'; database=weathermetrics_test;");
           
            return new ApplicationContext(builder.Options);
        }
    }
}