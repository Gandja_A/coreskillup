﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using WeatherMetrics.Domain.Core.Models;

namespace WeatherMetrics.Domain.Core
{
    public class ApplicationContext : IdentityDbContext<User, UserRole, int> 
    {
        public DbSet<ActionType> ActionTypes { get; set; }

        public DbSet<City> Cities { get; set; }

        public DbSet<LogAction> LogActions { get; set; }

        public DbSet<Dashboard> Dashboards { get; set; }

        public DbSet<UserRole> UserRole { get; set; }
      
        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<User>().HasOne(pt => pt.Dashboard);
          
            modelBuilder.Entity<DashboardCity>()
            .HasKey(t => new { t.CityId, t.DashboardId });

            modelBuilder.Entity<DashboardCity>()
                .HasOne(pt => pt.City)
                .WithMany(p => p.DashboardCities)
                .HasForeignKey(pt => pt.CityId);

            modelBuilder.Entity<DashboardCity>()
                .HasOne(pt => pt.Dashboard)
                .WithMany(t => t.DashboardCities)
                .HasForeignKey(pt => pt.DashboardId);
        }
    }
}