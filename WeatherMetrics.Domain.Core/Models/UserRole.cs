﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace WeatherMetrics.Domain.Core.Models
{
    public sealed class UserRole :  IdentityRole<int>
    {
        public UserRole() { }
        public UserRole(string name) { this.Name = name; }
    }
}