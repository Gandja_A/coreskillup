﻿using System;

namespace WeatherMetrics.Domain.Core.Models
{
    public class LogAction
    {
        public int Id { get; set; }
        public int? ActionTypeId { get; set; }
        public int? UserId { get; set; }

        public DateTime Date { get; set; }

        public ActionType ActionType { get; set; }

        public string Descriptions { get; set; }

        public virtual User User { get; set; }
    }
}