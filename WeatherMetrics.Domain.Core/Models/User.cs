﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace WeatherMetrics.Domain.Core.Models
{
    public class User :  IdentityUser<int>  //IdentityUser
    {
        [ForeignKey("Dashboar")]
        public int? DashboarId { get; set; }

        public virtual Dashboard Dashboard { get; set; }

        public virtual List<LogAction> Type { get; set; }
    }
}