﻿using System.Collections.Generic;

namespace WeatherMetrics.Domain.Core.Models
{
    public class City
    {
        public int Id { get; set; }

        public int? OpenWeatherId { get; set; }

        public string FullName { get; set; }

        public string Name { get; set; }

        public string Country { get; set; }

        public float Lon { get; set; }

        public float Lat { get; set; }

        public virtual List<DashboardCity> DashboardCities { get; set; }
    }
}