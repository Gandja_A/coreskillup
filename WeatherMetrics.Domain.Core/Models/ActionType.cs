﻿namespace WeatherMetrics.Domain.Core.Models
{
    public class ActionType
    {
        public int Id { get; set; }

        public string Descriptions { get; set; }
    }
}