﻿using System.Collections.Generic;

namespace WeatherMetrics.Domain.Core.Models
{
    public class Dashboard 
    {
        public int Id { get; set; }
        public int? UserId { get; set; }

        public virtual List<DashboardCity> DashboardCities { get; set; }

        public virtual User User { get; set; }
    }
}