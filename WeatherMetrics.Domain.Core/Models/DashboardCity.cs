﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WeatherMetrics.Domain.Core.Models
{
    public class DashboardCity
    {
        public int CityId { get; set; }
        public virtual City City { get; set; }

        public int DashboardId { get; set; }
        public virtual Dashboard Dashboard { get; set; }
    }
}
