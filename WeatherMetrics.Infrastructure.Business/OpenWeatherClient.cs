﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WeatherMetrics.Services.Interfaces;
using WeatherMetrics.Services.Interfaces.Models;

namespace WeatherMetrics.Infrastructure.Business
{
    public class OpenWeatherClient : IOpenWeatherClient
    {
        private const string ApiUrl = "http://api.openweathermap.org/data/2.5/";

        private const string ApiKey = "47c8a8a6793eabfc635736aa57375970";
        public async Task<WeatherModel> GetWeatherBySityName(string fullName)
        {   
            var url = string.Format("{0}weather?q={1}&units=metric&appid={2}", ApiUrl, fullName, ApiKey);
            var strResult = await this.WebClient(url);
            return JsonConvert.DeserializeObject<WeatherModel>(strResult);
        }

        public async Task<List<WeatherModel>> GetWeatherByIds(List<int> sitiesIds)
        {
            var jsonIds = string.Join(",", sitiesIds);
            var url = string.Format("{0}group?id={1}&units=metric&appid={2}", ApiUrl, jsonIds, ApiKey);
            var strResult = await this.WebClient(url);
            JObject jsobj = JObject.Parse(strResult);
            string value = jsobj.GetValue("list").ToString();
            var array = JsonConvert.DeserializeObject<WeatherModel[]>(value);
            return array.ToList();
        }

        public async Task<WeatherModel> GetWeatherByCoordinate(float lon, float lat)
        {
            var url = $"{ApiUrl}weather?lat={lon}&lon={lat}&units=metric&appid={ApiKey}";
            var strResult = await this.WebClient(url);
            return JsonConvert.DeserializeObject<WeatherModel>(strResult);
        }

        private async Task<string> WebClient(string url)
        {
            using (HttpClient client = new HttpClient())
            using (HttpResponseMessage response = await client.GetAsync(url))
            using (HttpContent content = response.Content)
            {
                var data = await content.ReadAsByteArrayAsync();
                string dataSrt = Encoding.UTF8.GetString(data, 0, data.Length);
                return dataSrt;
            }
        } 
    }
}