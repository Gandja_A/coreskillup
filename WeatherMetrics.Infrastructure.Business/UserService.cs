﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using WeatherMetrics.Domain.Core.Models;
using WeatherMetrics.Domain.Interfaces;
using WeatherMetrics.Services.Interfaces;
using WeatherMetrics.Services.Interfaces.Models;

namespace WeatherMetrics.Infrastructure.Business
{
    public class UserService : IUserService
    {
        private readonly IRepository<City> cityRep;

        private readonly IRepository<Dashboard> dashboardRep;

        private readonly IRepository<DashboardCity> dashboardCityRep;

        private readonly IRepository<LogAction> logActionRep;

        private readonly UserManager<User> userManager;

        public UserService(IRepository<City> cityRep, IRepository<Dashboard> dashboardRep,
            UserManager<User> userManager,
            IRepository<LogAction> logActionRep, IRepository<DashboardCity> dashboardCityRep)
        {
            this.cityRep = cityRep;
            this.dashboardRep = dashboardRep;
            this.userManager = userManager;
            this.logActionRep = logActionRep;
            this.dashboardCityRep = dashboardCityRep;
        }

        public List<UserCitiesModel> GetUsersList()
        {
            var returnResult = new List<UserCitiesModel>();
            //TODO need implement pagination
            // var userList = this.userListViewRep.GetAll().Skip(0).Take(1000).GroupBy(u=>u.UserName).ToList();

            var userList = (from user in this.userManager.Users
            join dash in this.dashboardRep.GetAll() on user.Id equals dash.User.Id
            into jsc
            from dash in jsc.DefaultIfEmpty(new Dashboard ())
            join dashCity in this.dashboardCityRep.GetAll() on dash.Id equals dashCity.DashboardId
            into jds
            from dashCity in jds.DefaultIfEmpty( new DashboardCity() )
            join city in this.cityRep.GetAll() on dashCity.CityId equals city.Id
            into jc
            from city in jc.DefaultIfEmpty( new City())
            select new
            {
                UserName = user.UserName,
                City = city
            }).ToList();

            foreach (var user in userList.GroupBy(u=>u.UserName))
            {
                var userCitiesModel = new UserCitiesModel
                {
                    UserName = user.Key
                };
                var userCities = user.Select(c => new CityModel
                {
                    CityName = c.City?.Name
                }).ToList();

                if (userCities != null && userCities.Any())
                {
                    userCitiesModel.Cities.AddRange(userCities);
                }

                returnResult.Add(userCitiesModel);
            }

            return returnResult;
        }

        public List<UserLogModel> GetUsersLogs()
        {
            //TODO need implement pagination
            var users =
                this.logActionRep.GetAll()
                    .Include(l => l.ActionType)
                    .Include(l => l.User)
                    .Select(l => l).ToList();
          
            var dbLogs = users.Select(l => new UserLogModel
            {
                    AtionType = l.ActionType.Descriptions,
                    Message = l.Descriptions,
                    LogTime = l.Date,
                    UserId = l.Id,
                    UserName = l.User.UserName
            }).ToList();

            return dbLogs;
        }
    }
}