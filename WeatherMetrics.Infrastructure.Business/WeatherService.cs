﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using WeatherMetrics.Services.Interfaces;
using WeatherMetrics.Services.Interfaces.Models;

namespace WeatherMetrics.Infrastructure.Business
{
    public class WeatherService : IWeatherService
    {
        private readonly IOpenWeatherClient openWeatherClient;

        private readonly IMemoryCache memoryCache;

        public WeatherService(IMemoryCache memoryCache, IOpenWeatherClient openWeatherClient)
        {
            this.memoryCache = memoryCache;
            this.openWeatherClient = openWeatherClient;
        }

        public List<WeatherModel> GetWeatherForecast(List<int> sitiesIds)
        {
            WeatherModel weatherModel;
            List<WeatherModel> resoult = new List<WeatherModel>();
            List<int> notCachingCitiesIds = new List<int>();
            foreach (var cityId in sitiesIds)
            {
                if (!this.memoryCache.TryGetValue(cityId, out weatherModel))
                {
                    notCachingCitiesIds.Add(cityId);
                }
                else
                {
                    resoult.Add(weatherModel);
                }
            }

            if (notCachingCitiesIds.Any())
            {
                List<WeatherModel> weatherResults = this.openWeatherClient.GetWeatherByIds(notCachingCitiesIds).Result;
                foreach (var weatherItem in weatherResults)
                {
                    this.memoryCache.Set(weatherItem.OpenWeatherCityId, weatherItem,
                          new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromMinutes(60)));
                }
                resoult.AddRange(weatherResults);
            }
            
            return resoult;
        }

        public WeatherModel GetWeatherForecast(string fullCityName)
        {
            WeatherModel returnResult;
            WeatherModel weatherResult = this.openWeatherClient.GetWeatherBySityName(fullCityName).Result;
            if (!this.memoryCache.TryGetValue(weatherResult.OpenWeatherCityId, out returnResult))
            {
                this.memoryCache.Set(weatherResult.OpenWeatherCityId, weatherResult,
                    new MemoryCacheEntryOptions().SetAbsoluteExpiration(TimeSpan.FromMinutes(60)));
                returnResult = weatherResult;
            }
            return returnResult;
        }
    }
}