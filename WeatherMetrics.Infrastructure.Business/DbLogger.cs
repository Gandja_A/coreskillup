﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Identity;
using WeatherMetrics.Domain.Core.Models;
using WeatherMetrics.Domain.Interfaces;
using WeatherMetrics.Services.Interfaces;
using WeatherMetrics.Services.Interfaces.Enum;

namespace WeatherMetrics.Infrastructure.Business
{
    public class DbLogger : IDbLog
    {
        private readonly IRepository<LogAction> logActionRep;

        private readonly UserManager<User> userManager;
        public DbLogger(IRepository<LogAction> logActionRep, UserManager<User> userManager)
        {
            this.logActionRep = logActionRep;
            this.userManager = userManager;
        }

        public void LogInformation(User user, ActionTypeEnum actionType, string msg)
        {
            this.logActionRep.Create(new LogAction
            {
                ActionTypeId = (int)actionType,
                User = user,
                Date = DateTime.UtcNow,
                Descriptions = msg
            });
            this.logActionRep.Save();
        }

        public void LogError(Exception exp)
        {
            this.logActionRep.Create(new LogAction
            {
                ActionTypeId = (int)ActionTypeEnum.Exception,
                Date = DateTime.UtcNow,
                Descriptions = exp.Message
            });
            this.logActionRep.Save();
        }

        public void LogError(string userName, Exception exp)
        {
            var dbUser = this.userManager.Users.FirstOrDefault(u => u.UserName == userName);

            this.logActionRep.Create(new LogAction
            {
                ActionTypeId = (int)ActionTypeEnum.Exception,
                Date = DateTime.UtcNow,
                Descriptions = exp.Message,
                User = dbUser
            });
            this.logActionRep.Save();
        }
    }
}