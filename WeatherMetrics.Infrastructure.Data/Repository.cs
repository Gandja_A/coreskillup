﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using WeatherMetrics.Domain.Core;
using WeatherMetrics.Domain.Interfaces;


namespace WeatherMetrics.Infrastructure.Data
{
    public class Repository<T> : IRepository<T> where T : class
    {

        protected readonly ApplicationContext dbContext;

        protected DbSet<T> DbSet;

        private bool disposed = false;

        public Repository(ApplicationContext dbContext)
        {
            this.dbContext = dbContext;
            this.DbSet =  this.dbContext.Set<T>();
        }

        public IQueryable<T> GetAll()
        {
            return this.DbSet;
        }
        
        public void Create(T item)
        {
            this.DbSet.Add(item);
        }

        public void Update(T item)
        {
            this.dbContext.Entry(item).State = EntityState.Modified;
        }

        public void Delete(T item)
        {
            var dbEntityEntry = this.dbContext.Entry<T>(item);
            if (dbEntityEntry.State == EntityState.Detached)
            {
                this.DbSet.Attach(item);
            }
            this.DbSet.Remove(item);
        }
        
        public void Save()
        {
            this.dbContext.SaveChanges();
        }

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    this.dbContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
