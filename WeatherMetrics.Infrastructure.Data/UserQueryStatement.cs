﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using WeatherMetrics.Domain.Core;
using WeatherMetrics.Domain.Core.Models;
using WeatherMetrics.Domain.Interfaces;


namespace WeatherMetrics.Infrastructure.Data
{
    public class UserQueryStatement : IUserQueryStatement
    {
        private readonly IRepository<DashboardCity> dashboardCityRep;

        private readonly UserManager<User> userManager;

        public UserQueryStatement(IRepository<DashboardCity> dashboardCityRep, UserManager<User> userManager)
        {
            this.dashboardCityRep = dashboardCityRep;
            this.userManager = userManager;
        }

        public List<City> GetAllUserCities(string userName)
        {
            var dbUser = this.userManager.Users.Include(u => u.Dashboard).FirstOrDefault(u => u.UserName == userName);
            List<City> dashboardCityList = this.dashboardCityRep.GetAll().Where(d => dbUser.Dashboard != null && d.DashboardId ==  dbUser.Dashboard.Id)
                    .Select(c => c.City).ToList();
            return dashboardCityList;
        }
    }
}