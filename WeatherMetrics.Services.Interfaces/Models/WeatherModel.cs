﻿using Newtonsoft.Json;

namespace WeatherMetrics.Services.Interfaces.Models
{
    public class WeatherModel
    {
        [JsonProperty("id")]
        public int OpenWeatherCityId { get; set; }

        [JsonProperty("name")]
        public string CityName { get; set; }

        [JsonProperty("main")]
        public TemperatureModel Temperature { get; set; }
    }
}