﻿namespace WeatherMetrics.Services.Interfaces.Models
{
    public class CityModel
    {
        public string CityName { get; set; }
    }
}