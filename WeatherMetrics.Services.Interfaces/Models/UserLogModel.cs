﻿using System;

namespace WeatherMetrics.Services.Interfaces.Models
{
    public class UserLogModel
    {
        public int? UserId  { get; set; }

        public string UserName { get; set; }

        public DateTime? LogTime { get; set; }

        public string AtionType { get; set; }

        public string Message { get; set; }
    }
}