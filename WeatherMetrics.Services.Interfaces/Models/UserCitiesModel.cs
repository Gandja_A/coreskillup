﻿using System.Collections.Generic;

namespace WeatherMetrics.Services.Interfaces.Models
{
    public class UserCitiesModel
    {
        public string UserName { get; set; }
        public List<CityModel> Cities { get; set; }

        public UserCitiesModel()
        {
            this.Cities = new List<CityModel>();
        }
    }
}