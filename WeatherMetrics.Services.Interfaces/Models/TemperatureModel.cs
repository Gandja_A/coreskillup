﻿using Newtonsoft.Json;

namespace WeatherMetrics.Services.Interfaces.Models
{
    public class TemperatureModel
    {
        [JsonProperty("temp")]
        public float Temp { get; set; }

        [JsonProperty("temp_min")]
        public float Min { get; set; }

        [JsonProperty("temp_max")]
        public float Max { get; set; }
    }
}