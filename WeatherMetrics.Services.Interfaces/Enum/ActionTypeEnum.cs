﻿namespace WeatherMetrics.Services.Interfaces.Enum
{
    public enum ActionTypeEnum
    {
        UserCreatedAnAaccount = 1,

        UserLoggedIn =2,

        UserLoggedOut =3,

        ChangedCitiesList = 4,

        Exception = 5
    }
}