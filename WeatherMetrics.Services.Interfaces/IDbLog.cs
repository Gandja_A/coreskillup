﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WeatherMetrics.Domain.Core.Models;
using WeatherMetrics.Services.Interfaces.Enum;

namespace WeatherMetrics.Services.Interfaces
{
    public interface IDbLog
    {
        void LogInformation(User user, ActionTypeEnum actionType, string msg);
        void LogError(Exception exp);
        void LogError(string userName, Exception exp);
    }
}
