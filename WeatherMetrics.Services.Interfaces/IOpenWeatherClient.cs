﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WeatherMetrics.Services.Interfaces.Models;

namespace WeatherMetrics.Services.Interfaces
{
    public interface IOpenWeatherClient
    {
        Task<WeatherModel> GetWeatherBySityName(string fullName);

        Task<List<WeatherModel>> GetWeatherByIds(List<int> sitiesIds);

        Task<WeatherModel> GetWeatherByCoordinate(float lon, float lat);
    }
}