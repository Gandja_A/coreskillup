﻿using System.Collections.Generic;
using WeatherMetrics.Services.Interfaces.Models;

namespace WeatherMetrics.Services.Interfaces
{
    public interface IWeatherService
    {
         List<WeatherModel> GetWeatherForecast(List<int> sitiesIds);
         WeatherModel GetWeatherForecast(string fullCityName);
    }
}