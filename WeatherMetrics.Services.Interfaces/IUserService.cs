﻿using System.Collections.Generic;
using WeatherMetrics.Services.Interfaces.Models;

namespace WeatherMetrics.Services.Interfaces
{
    public interface IUserService
    {
        List<UserCitiesModel> GetUsersList();

        List<UserLogModel> GetUsersLogs();
    }
}