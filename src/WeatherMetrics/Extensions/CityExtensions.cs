﻿using WeatherMetrics.Domain.Core.Models;
using WeatherMetrics.Models;

namespace WeatherMetrics.Extensions
{
    public static class CityExtensions
    {
        public static CityModel GetCityModel(this City dbCity)
        {
            var cityModel = new CityModel
            {
                Id = dbCity.Id,
                Name = dbCity.Name,
                FullName = dbCity.FullName,
                Country = dbCity.Country,
                Lon = dbCity.Lon,
                Lat = dbCity.Lat
            };

            return cityModel;
        }
    }
}
