﻿using System;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace WeatherMetrics.Utils.Binders
{
    public class InvariantFloatModelBinderProvider : IModelBinderProvider
    {
        public IModelBinder GetBinder(ModelBinderProviderContext context)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));

            if (!context.Metadata.IsComplexType && (context.Metadata.ModelType == typeof(float) || context.Metadata.ModelType == typeof(float?)))
            {
                return new InvariantFloatModelBinder(context.Metadata.ModelType);
            }

            return null;
        }
    }
}