﻿using System.Collections.Generic;
using WeatherMetrics.Services.Interfaces.Enum;

namespace WeatherMetrics.Utils.Startup
{
    public static class DbInitHelper
    {
        public static readonly Dictionary<ActionTypeEnum, string> ActionTypes =

            new Dictionary<ActionTypeEnum, string>
            {
                    {ActionTypeEnum.UserCreatedAnAaccount, "User created an account"},
                    {ActionTypeEnum.UserLoggedIn, "User logged in"},
                    {ActionTypeEnum.UserLoggedOut, "User Logged Out"},
                    {ActionTypeEnum.ChangedCitiesList, "Changed Cities List"},
                    {ActionTypeEnum.Exception, "Exception was generated"}
            };
    }
}