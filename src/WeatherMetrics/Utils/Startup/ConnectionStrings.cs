﻿namespace WeatherMetrics.Utils.Startup
{
    public static class ConnectionStrings
    {
        public const string MYSQL_DEFAULT_CONNECTION = "MySqlDefaultConnection";
        public const string MYSQL_STAGING_CONNECTION = "MySqlStagingConnection";
        public const string MSSQL_DEFAULT_CONNECTION = "MsSqlDefaultConnection";
        public const string MSSQL_STAGING_CONNECTION = "MsSqlStagingConnection";
    }
}
