﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using WeatherMetrics.Domain.Core.Models;

namespace WeatherMetrics.Models
{
    public class CityModel
    {
        public int Id { get; set; }
     
        public string Name { get; set; }

        public string FullName { get; set; }

        public string Country { get; set; }

        public float Lon { get; set; }

        public float Lat { get; set; }

        public CityModel()
        {
        }
    }
}