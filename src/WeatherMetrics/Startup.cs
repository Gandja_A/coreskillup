﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration.UserSecrets;
using Microsoft.Extensions.Options;
using WeatherMetrics.Domain.Core;
using WeatherMetrics.Domain.Core.Models;
using WeatherMetrics.Domain.Interfaces;
using WeatherMetrics.Infrastructure.Business;
using WeatherMetrics.Infrastructure.Data;
using WeatherMetrics.Services.Interfaces;
using WeatherMetrics.Services.Interfaces.Enum;
using FluentValidation.AspNetCore;
using WeatherMetrics.Utils.Binders;
using WeatherMetrics.Utils.Startup;


[assembly: UserSecretsId("Ms-sql-A2F3C3DE-6128-4441-8252-3BCB39C269F0")]

namespace WeatherMetrics
{
    public class Startup
    {
        private bool IsMySql = false;

        private IHostingEnvironment hostingEnvironment;
        public IConfigurationRoot Configuration { get; }

        public Startup(IHostingEnvironment env)
        {
            this.hostingEnvironment = env;
            string commandLineArg = Environment.GetCommandLineArgs().Skip(1).ToList().Skip(1).FirstOrDefault();
            this.IsMySql = commandLineArg != null && commandLineArg.Equals("mysql");
            
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            
            if (env.IsDevelopment())
            {
                builder.AddUserSecrets();
                builder.AddApplicationInsightsSettings(developerMode: true);
            }
      
            this.Configuration = builder.Build();
        }
        
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddApplicationInsightsTelemetry(this.Configuration);
          
            if (this.IsMySql)
            {
                services.AddDbContext<ApplicationContext>(options => options.UseMySql(this.GetConnectionString(), b => b.MigrationsAssembly("WeatherMetrics.Domain.Core")));
            }
            else
            {
                services.AddDbContext<ApplicationContext>(options => options.UseSqlServer(this.GetConnectionString(), b => b.MigrationsAssembly("WeatherMetrics.Domain.Core")));
            }

            services.AddIdentity<User, UserRole>(o => {
                o.Password.RequireDigit = false;
                o.Password.RequireLowercase = false;
                o.Password.RequireUppercase = false;
                o.Password.RequireNonAlphanumeric = false;
                o.Password.RequiredLength = 8;
            }).AddEntityFrameworkStores<ApplicationContext, int>().AddDefaultTokenProviders();

         
            services.AddDistributedMemoryCache();
            services.AddSession();
     
            services.Configure<RequestLocalizationOptions>(
             options =>
             {
                 var supportedCultures = new List<CultureInfo>
                     {
                            new CultureInfo("en-US"),
                            new CultureInfo("ru-RU")
                     };

                 options.DefaultRequestCulture = new RequestCulture(culture: "en-US", uiCulture: "en-US");
                 options.SupportedCultures = supportedCultures;
                 options.SupportedUICultures = supportedCultures;
             });

            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddLocalization(options => options.ResourcesPath = "Resources");

            services.AddMvc()
            .AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<Startup>())
            // Add support for finding localized views, based on file name suffix, e.g. Index.fr.cshtml
            .AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix)
            // Add support for localizing strings in data annotations (e.g. validation messages) via the
            // IStringLocalizer abstractions.
            .AddDataAnnotationsLocalization().AddMvcOptions(options => {
                options.ModelBinderProviders.Insert(0, new InvariantFloatModelBinderProvider());
            });

            services.AddTransient<IEmailSender, EmailService>();
            services.AddTransient<IEmailSender, EmailService>();
            services.AddScoped<IOpenWeatherClient, OpenWeatherClient>();
            services.AddScoped<IWeatherService, WeatherService>();
            services.AddScoped<IUserQueryStatement, UserQueryStatement>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IDbLog, DbLogger>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(this.Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            // app.UseApplicationInsightsExceptionTelemetry();

            // app.UseStatusCodePages();

            app.UseStaticFiles();

            app.UseSession();

            app.UseIdentity();
            
            var options = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();

            app.UseRequestLocalization(options.Value);

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            this.DatabaseInitialize(app.ApplicationServices).Wait();
        }

        public async Task DatabaseInitialize(IServiceProvider serviceProvider)
        {
            UserManager<User> userManager =
                serviceProvider.GetRequiredService<UserManager<User>>();
            RoleManager<UserRole> roleManager =
                serviceProvider.GetRequiredService<RoleManager<UserRole>>();

            var actionTypeRep = serviceProvider.GetRequiredService<IRepository<ActionType>>();

            string adminEmail = "admin@gmail.com";

            string password = "12345678";

            if (await roleManager.FindByNameAsync("Admin") == null)
            {
                await roleManager.CreateAsync(new UserRole("Admin"));
            }

            if (await roleManager.FindByNameAsync("User") == null)
            {
                await roleManager.CreateAsync(new UserRole("User"));
            }

            foreach ( KeyValuePair<ActionTypeEnum, string> actionType in DbInitHelper.ActionTypes)
            {
                var existActionType = actionTypeRep.GetAll().FirstOrDefault(a => a.Descriptions == actionType.Value);
                if (existActionType == null)
                {
                    actionTypeRep.Create(new ActionType { Descriptions = actionType.Value });
                    actionTypeRep.Save();
                }
            }

            if (await userManager.FindByNameAsync(adminEmail) == null)
            {
                User admin = new User { Email = adminEmail, UserName = adminEmail };
                IdentityResult result = await userManager.CreateAsync(admin, password);
                if (result.Succeeded)
                {
                    await userManager.AddToRoleAsync(admin, "Admin");
                }
            }
        }

        private string GetConnectionString()
        {
            if (this.hostingEnvironment.IsDevelopment())
            {
                return this.IsMySql ? this.Configuration[ConnectionStrings.MYSQL_DEFAULT_CONNECTION] : this.Configuration[ConnectionStrings.MSSQL_DEFAULT_CONNECTION];
            }
            else
            {
                string connectionName;
                if (this.hostingEnvironment.IsProduction())
                {
                    connectionName = this.IsMySql ? ConnectionStrings.MYSQL_DEFAULT_CONNECTION : ConnectionStrings.MSSQL_DEFAULT_CONNECTION;
                }
                else
                {
                    connectionName = this.IsMySql ? ConnectionStrings.MYSQL_STAGING_CONNECTION : ConnectionStrings.MSSQL_STAGING_CONNECTION;
                }
                return this.Configuration.GetConnectionString(connectionName);
            }
        }
    }
}
