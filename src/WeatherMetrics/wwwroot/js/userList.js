﻿$(document).ready(function () {
    var grid;
    var grid = $('#grid').grid({
        uiLibrary: 'bootstrap',
        //dataSource: data,
        columns: [
            { field: 'userName', sortable: true }
            //{ field: 'Name', sortable: true },
            //{ field: 'PlaceOfBirth', sortable: true }
        ],
        detailTemplate: '<div><table/></div>',
        pager: { limit: 5, sizes: [2, 5, 10, 20] }
    });
    grid.on('detailExpand', function (e, $detailWrapper, record) {
        $detailWrapper.find('table').grid({
            dataSource: record.cities,
            columns: [{ field: 'cityName', title: 'City Name' }],
            pager: { limit: 5 }
        });
    });
    grid.on('detailCollapse', function (e, $detailWrapper, record) {
        $detailWrapper.find('table').grid('destroy', true, true);
    });
});