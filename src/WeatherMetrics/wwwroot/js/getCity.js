﻿var grid;
$(document).ready(function () {
       grid = $('#grid').grid({
        uiLibrary: 'bootstrap',
        columns: [
            { field: 'id', width: 64 },
            { field: 'name', sortable: true },
            { field: 'country', sortable: true },
            { field: 'lon', sortable: true },
            { field: 'lat', sortable: true },
            { title: '', field: 'Delete', width: 34, type: 'icon', icon: 'glyphicon-remove', tooltip: 'Delete', events: { 'click': Delete } }
        ],
        pager: { limit: 5, sizes: [2, 5, 10, 20] }
    });

       function Delete(e) {
           var deleteRow = grid.getById(e.data.id);
           if (confirm('The city ' + deleteRow.name+ ' will be deleted, Are you sure?')) {
            debugger;
            $.post("/api/apiCity/delete", { cityId: deleteRow.id },
                function (data) {
                    grid.removeRow(e.data.id - 1);
                });
  
            //grid.reload(); 
        }
    }   

    $('#btnSearch').on('click', function () {
        grid.reload({ name: $('#txtQuery').val() });
    });
    $('#btnClear').on('click', function () {
        $('#txtQuery').val('');
        grid.reload({ name: '' });
    });
});
///==================================google map section=========================================///
var cityModel = {};
function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
        center: { lat: 49.9935, lng: 36.23038 },
        zoom: 13
    });

    var input = document.getElementById('pac-input');

    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);

    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    var contentString = '<div cl-md-12><button type="button" id="btnAddCity" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-plus-sign" aria-hidden="true">' +
        '</span>Add</button></div>';

    var infowindow = new google.maps.InfoWindow({ content: contentString });
    var marker = new google.maps.Marker({
        map: map
    });
    marker.addListener('click', function () {
        infowindow.open(map, marker);
    });

    autocomplete.addListener('place_changed', function () {
        infowindow.close();
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            return;
        }

        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);
        }

        // Set the position of the marker using the place ID and location.
        marker.setPlace({
            placeId: place.place_id,
            location: place.geometry.location
        });
        marker.setVisible(true);

        cityModel.Lon = place.geometry.location.lng();
        cityModel.Lat = place.geometry.location.lat();
        cityModel.Name = place.name;
        cityModel.Country = place.address_components[place.address_components.length -1].long_name;
        cityModel.FullName = place.formatted_address;

       infowindow.open(map, marker);

        $('#btnAddCity').on('click', function () {
            debugger;
            $.post("/api/apiCity/add", cityModel,
                function (data) {
                    debugger;
                    grid.addRow({ 'id': data.id, 'name': data.name, 'country': data.country, 'lon': data.lon, 'lat': data.lat });
                    marker.setVisible(false);
                    infowindow.close();
                });
        });
    });
}

