﻿$(document).ready(function () {
    var grid = $('#grid').grid({
        columns: [
            { field: 'userId' },
            { field: 'userName' },
            { field: 'logTime', sortable: true },
            { field: 'ationType', sortable: true },
            { field: 'message', sortable: true }
        ],
        pager: { limit: 5, sizes: [2, 5, 10, 20] }
    });

    $('#btnSearch').on('click', function () {
        grid.reload({ name: $('#txtQuery').val() });
    });
    $('#btnClear').on('click', function () {
        $('#txtQuery').val('');
        grid.reload({ name: '' });
    });
});