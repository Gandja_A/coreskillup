﻿namespace WeatherMetrics.ViewModels.Dashboard
{
    public class WeatherViewModel
    {
        public string CityName { get; set; }

        public float Temp { get; set; }
        
        public float TempMin { get; set; }

        public float TempMax { get; set; }
    }
}