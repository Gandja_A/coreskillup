﻿using System;
using WeatherMetrics.Services.Interfaces.Models;

namespace WeatherMetrics.ViewModels.UserLogs
{
    public class UserLogViewModel
    {
        public int? UserId { get; set; }

        public string UserName { get; set; }

        public DateTime? LogTime { get; set; }

        public string AtionType { get; set; }

        public string Message { get; set; }

        public UserLogViewModel()
        {
        }
        public UserLogViewModel(UserLogModel userLog)
        {
            this.UserId = userLog.UserId;
            this.LogTime = userLog.LogTime;
            this.AtionType = userLog.AtionType;
            this.Message = userLog.Message;
            this.UserName = userLog.UserName;
        }
    }
}