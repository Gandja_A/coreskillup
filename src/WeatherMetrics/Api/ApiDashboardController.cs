﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WeatherMetrics.Domain.Core.Models;
using WeatherMetrics.Domain.Interfaces;
using WeatherMetrics.ExceptionFilter;
using WeatherMetrics.Services.Interfaces;
using WeatherMetrics.Services.Interfaces.Models;

namespace WeatherMetrics.Api
{
    [Authorize(Roles = "User")]
    [Route("api/[controller]")]
    [TypeFilter(typeof(CustomExceptionFilter))]
    public class ApiDashboardController : Controller
    {
        private readonly IUserQueryStatement userQueryStatement;

        private readonly IWeatherService weatherService;

        public ApiDashboardController(IWeatherService weatherService, IUserQueryStatement userQueryStatement)
        {
            this.weatherService = weatherService;
            this.userQueryStatement = userQueryStatement;
        }

        [Route("getDashboardData")]
        public IActionResult GetDashboardData()
        {
            var loginUserName = this.HttpContext.User.Identity.Name;
            List<int> cityIdList = this.userQueryStatement.GetAllUserCities(loginUserName).Select(c => c.OpenWeatherId ?? 0).ToList();
            List<WeatherModel> result = this.weatherService.GetWeatherForecast(cityIdList);
            return this.Json(result);
        }
    }
}
