﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WeatherMetrics.ExceptionFilter;
using WeatherMetrics.Services.Interfaces;
using WeatherMetrics.Services.Interfaces.Models;
using WeatherMetrics.ViewModels.UserLogs;

namespace WeatherMetrics.Api
{
    [Authorize(Roles = "Admin")]
    [TypeFilter(typeof(CustomExceptionFilter))]
    [Route("api/[controller]")]
    public class ApiAdminController : Controller
    {
        private readonly IUserService userService;

        public ApiAdminController(IUserService userService)
        {
            this.userService = userService;
        }

        [Route("getUsersLogs")]
        public IActionResult GetUsersLogs()
        {
           var logs = this.userService.GetUsersLogs().Select( l=> new UserLogViewModel(l));
           return this.Json(logs);
        }

        [Route("users")]
        public IActionResult GetUsersList()
        {
            List<UserCitiesModel> result = this.userService.GetUsersList();
            return this.Json(result);
        }
    }
}
