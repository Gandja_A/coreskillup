﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WeatherMetrics.Domain.Core.Models;
using WeatherMetrics.Domain.Interfaces;
using WeatherMetrics.ExceptionFilter;
using WeatherMetrics.Extensions;
using WeatherMetrics.Models;
using WeatherMetrics.Services.Interfaces;

namespace WeatherMetrics.Api
{
    [Authorize(Roles = "User")]
    [TypeFilter(typeof(CustomExceptionFilter))]
    [Route("api/[controller]")]
    public class ApiCityController : Controller
    {
        private readonly IRepository<City> cityRep;

        private readonly IRepository<Dashboard> dashboardRep;

        private readonly IRepository<DashboardCity> dashboardCityRep;

        private readonly UserManager<User> userManager;

        private readonly IWeatherService weatherService;

        public ApiCityController(IRepository<City> cityRep, IRepository<Dashboard> dashboardRep,
            UserManager<User> userManager, IRepository<DashboardCity> dashboardCityRep, IWeatherService weatherService)
        {
            this.cityRep = cityRep;
            this.dashboardRep = dashboardRep;
            this.userManager = userManager;
            this.dashboardCityRep = dashboardCityRep;
            this.weatherService = weatherService;
        }

        [Route("get")]
        public IActionResult GetCity(int page, int limit, string name, string sortBy, string direction = "asc")
        {
            var loginUserName = this.HttpContext.User.Identity.Name;
            var dbUser =
                this.userManager.Users.Include(u => u.Dashboard).FirstOrDefault(u => u.UserName == loginUserName);
            var cityList = this.dashboardCityRep.GetAll().Where(d => dbUser.Dashboard != null && d.DashboardId == dbUser.Dashboard.Id).Select(c =>
                new CityModel
                {
                    Country = c.City.Country,
                    Id = c.City.Id,
                    Lat = c.City.Lat,
                    Lon = c.City.Lon,
                    Name = c.City.Name
                });

            var jsonResult = new
            {
                records = cityList,
            };
            return this.Ok(jsonResult);
        }

        [Route("add")]
        public IActionResult AddCity(CityModel city)
        {
            var loginUserName = this.HttpContext.User.Identity.Name;
            var dbUser =
                this.userManager.Users.Include(u => u.Dashboard).FirstOrDefault(u => u.UserName == loginUserName);
            City dbCity = this.cityRep.GetAll().FirstOrDefault(c => c.Country == city.Country && c.Name == city.Name);

            if (dbCity == null)
            {
                var cityId = this.weatherService.GetWeatherForecast(city.FullName)?.OpenWeatherCityId;
                dbCity = new City
                {
                    Country = city.Country,
                    Name = city.Name,
                    FullName = city.FullName,
                    Lat = city.Lat,
                    Lon = city.Lon,
                    OpenWeatherId = cityId
                };
                this.cityRep.Create(dbCity);
                this.cityRep.Save();
            }

            if (dbUser.Dashboard == null)
            {
                dbUser.Dashboard = new Dashboard
                {
                    User = dbUser,
                    DashboardCities = new List<DashboardCity>
                    {
                        new DashboardCity
                        {
                            City = dbCity
                        }
                    }
                };
                this.dashboardRep.Save();
                return this.Ok(dbCity);
            }

            var dbUserDashboard =
                this.dashboardRep.GetAll().Include(d => d.DashboardCities).FirstOrDefault(d => d.User.Id == dbUser.Id);
            if (dbUserDashboard != null && dbUserDashboard.DashboardCities.All(d => d.CityId != dbCity.Id))
            {
                dbUserDashboard.DashboardCities.Add(new DashboardCity {City = dbCity});
                this.dashboardRep.Save();
            }
            return this.Ok(dbCity.GetCityModel());
        }

        [Route("delete")]
        public IActionResult DeleteCity(int cityId)
        {
            try
            {
                var deleteCity = new City {Id = cityId };
                this.cityRep.Delete(deleteCity);
                this.cityRep.Save();
            }
            catch (Exception)
            {
                return this.BadRequest();
            }
            return this.Ok();
        }
    }
}
