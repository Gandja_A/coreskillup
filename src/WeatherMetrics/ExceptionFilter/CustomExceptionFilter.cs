﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using WeatherMetrics.Services.Interfaces;

namespace WeatherMetrics.ExceptionFilter
{
    public class CustomExceptionFilter : Attribute, IExceptionFilter
    {
        private readonly IDbLog dblogger;
        public CustomExceptionFilter(IDbLog dblogger)
        {
            this.dblogger = dblogger;
        }

        public void OnException(ExceptionContext context)
        {
            var userName = context.HttpContext.User.Identity.Name;
            this.dblogger.LogError(userName, context.Exception);
        }
    }
}