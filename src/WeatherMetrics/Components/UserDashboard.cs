﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using WeatherMetrics.Domain.Core.Models;
using WeatherMetrics.Domain.Interfaces;
using WeatherMetrics.Services.Interfaces;
using WeatherMetrics.Services.Interfaces.Models;
using WeatherMetrics.ViewModels.Dashboard;

namespace WeatherMetrics.Components
{
    public class UserDashboard : ViewComponent
    {
        private readonly IUserQueryStatement userQueryStatement;

        private readonly IWeatherService weatherService;

        public UserDashboard(IWeatherService weatherService, IUserQueryStatement userQueryStatement)
        {
            this.weatherService = weatherService;
            this.userQueryStatement = userQueryStatement;
        }

        public IViewComponentResult Invoke(string userName)
        {
            List<int> cityIdList = this.userQueryStatement.GetAllUserCities(userName).Select(c => c.OpenWeatherId ?? 0).ToList();
            IEnumerable<WeatherViewModel> result = this.weatherService.GetWeatherForecast(cityIdList).Select(c => new WeatherViewModel
            {
                CityName = c.CityName,
                Temp = c.Temperature.Temp,
                TempMax = c.Temperature.Max,
                TempMin = c.Temperature.Min
            } );
            return this.View(result);
        }
    }
}