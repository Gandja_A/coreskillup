﻿using FluentValidation;
using Microsoft.Extensions.Localization;
using WeatherMetrics.ViewModels.AccountViewModels;

namespace WeatherMetrics.Validation.Account
{
    public class LoginViewModelValidator : AbstractValidator<LoginViewModel>
    {
        private readonly IStringLocalizer<LoginViewModel> localizer;

        public LoginViewModelValidator(IStringLocalizer<LoginViewModel> localizer)
        {
            this.localizer = localizer;
            this.RuleFor(x => x.Email).NotEmpty().WithMessage(this.localizer["Error Message"]);
            this.RuleFor(x => x.Password).NotEmpty(). WithMessage("Error Message");
        }
    }
}