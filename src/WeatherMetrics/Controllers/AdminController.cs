﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WeatherMetrics.ExceptionFilter;

namespace WeatherMetrics.Controllers
{
    [Authorize(Roles = "Admin")]
    [TypeFilter(typeof(CustomExceptionFilter))]
    public class AdminController : Controller
    {
        [HttpGet]
        public IActionResult UsersList()
        {
            return this.View("UsersList");
        }

        [HttpGet]
        public IActionResult UserLogs()
        {
            return this.View("UserLogs");
        }
    }
}
