﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WeatherMetrics.ExceptionFilter;

namespace WeatherMetrics.Controllers
{
    [Authorize(Roles = "User")]
    [TypeFilter(typeof(CustomExceptionFilter))]
    public class UserController : Controller
    {
        [HttpGet]
        public IActionResult CitiesManagement()
        {
            return this.View("CitiesManagement");
        }

        [HttpGet]
        public IActionResult Dashboard()
        {
            return this.View("Dashboard");
        }
    }
}
