﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using WeatherMetrics.Domain.Core.Models;
using WeatherMetrics.ExceptionFilter;
using WeatherMetrics.Services.Interfaces;
using WeatherMetrics.Services.Interfaces.Enum;
using WeatherMetrics.ViewModels.AccountViewModels;

namespace WeatherMetrics.Controllers
{
    [TypeFilter(typeof(CustomExceptionFilter))]
    public class AccountController : Controller
    {
        private readonly UserManager<User> userManager;

        private readonly SignInManager<User> signInManager;

        private readonly IEmailSender emailSender;

        private readonly IDbLog dbLogger;

        public AccountController(UserManager<User> userManager, SignInManager<User> signInManager, IEmailSender emailSender, IDbLog dbLogger)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.emailSender = emailSender;
            this.dbLogger = dbLogger;
        }

        [HttpGet]
        public IActionResult Register()
        {
            return this.View();
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel viewModel)
        {
            if (this.ModelState.IsValid)
            {
                User user = new User { Email = viewModel.Email, UserName = viewModel.UserName, Roles = { }};
                
                var result = await this.userManager.CreateAsync(user, viewModel.Password);
                if (result.Succeeded)
                {
                    await this.userManager.AddToRoleAsync(user, "User");
                    await this.signInManager.SignInAsync(user, false);
                    this.dbLogger.LogInformation(user, ActionTypeEnum.UserCreatedAnAaccount, "User Created An Account");
                    return this.RedirectToAction("Dashboard", "User");
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        this.ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
            }
            return this.View(viewModel);
        }

        [HttpGet]
        public IActionResult Login(string returnUrl = null)
        {
            this.ViewData["ReturnUrl"] = returnUrl;
            return this.View(new LoginViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel viewModel, string returnUrl = null)
        {
            this.ViewData["ReturnUrl"] = returnUrl;
            if (this.ModelState.IsValid)
            {
                var user = await this.userManager.FindByEmailAsync(viewModel.Email);
                var result =
                    await this.signInManager.PasswordSignInAsync(user.UserName, viewModel.Password, viewModel.RememberMe, false);
              
                if (result.Succeeded)
                {
                    this.dbLogger.LogInformation(user, ActionTypeEnum.UserLoggedIn, "User Logged In");
                    if (!String.IsNullOrEmpty(returnUrl) && this.Url.IsLocalUrl(returnUrl))
                    {
                        return this.Redirect(returnUrl);
                    }

                    return this.RedirectToAction("Index", "Home");
                }
               
                this.ModelState.AddModelError("", "Неправильный логин и (или) пароль");
            }
            return this.View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LogOff()
        {
            var userName = this.HttpContext.User.Identity.Name;
            var user = await this.userManager.FindByNameAsync(userName);
            await this.signInManager.SignOutAsync();
            this.dbLogger.LogInformation(user, ActionTypeEnum.UserLoggedOut, "User Logged Out");
            return this.RedirectToAction("Index", "Home");
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPassword()
        {
            return this.View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                var user = await this.userManager.FindByEmailAsync(model.Email);
                if (user == null ) //|| !(await this.userManager.IsEmailConfirmedAsync(user))
                {
                    return this.View("ForgotPasswordConfirmation");
                }

                var code = await this.userManager.GeneratePasswordResetTokenAsync(user);
                var callbackUrl = this.Url.Action("ResetPassword", "Account", new { userId = user.Id, code = code }, protocol: this.HttpContext.Request.Scheme);
               // EmailService emailService = new EmailService();
                await this.emailSender.SendEmailAsync(model.Email, "Reset Password",
                    $"Для сброса пароля пройдите по ссылке: <a href='{callbackUrl}'>link</a>");
                return this.View("ForgotPasswordConfirmation");
            }
            return this.View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPassword(string code = null)
        {
            return code == null ? this.View("Error") : View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return View(model);
            }
            var user = await this.userManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                return this.RedirectToAction(nameof(this.ResetPasswordConfirmation), "Account");
            }
            var result = await this.userManager.ResetPasswordAsync(user, model.Code, model.Password);
            if (result.Succeeded)
            {
                return this.RedirectToAction(nameof(this.ResetPasswordConfirmation), "Account");
            }
            this.AddErrors(result);
            return this.View();
        }

        [AllowAnonymous]
        public IActionResult ResetPasswordConfirmation()
        {
            return this.View();
        }
        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                this.ModelState.AddModelError(string.Empty, error.Description);
            }
        }
    }
}