﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using WeatherMetrics.ExceptionFilter;

namespace WeatherMetrics.Controllers
{
    [TypeFilter(typeof(CustomExceptionFilter))]
    public class HomeController : Controller
    {
        public IActionResult Index()
        { 
            return this.View("Index");
        }
      
        public IActionResult Error()
        {
            return this.View();
        }

        [HttpPost]
        public IActionResult SetLanguage(string culture, string returnUrl)
        {
            this.Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
            );
            
            return this.LocalRedirect(returnUrl);
        }
    }
}
