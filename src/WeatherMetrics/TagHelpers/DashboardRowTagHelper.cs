﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Razor.TagHelpers;
using WeatherMetrics.ViewModels.Dashboard;

namespace WeatherMetrics.TagHelpers
{
    public class DashboardRowTagHelper : TagHelper
    {
        public WeatherViewModel weather { get; set; }
        public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {
            string listContent = "";
            output.TagName = "tr";

            listContent += "<td>" + this.weather.CityName + "</td>";
            listContent += "<td>" + this.weather.Temp + "</td>";
            listContent += "<td>" + this.weather.TempMax + "</td>";
            listContent += "<td>" + this.weather.TempMin + "</td>";
            output.TagMode = TagMode.StartTagAndEndTag;
            output.Content.SetHtmlContent(listContent);
        }
    }
}